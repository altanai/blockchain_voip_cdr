    // connect to RSK Local node using the wallet injected, ie via Metamask:
    // web3 provider with fallback for old version
    window.addEventListener('load', async () => {
        // New web3 provider
        if (window.ethereum) {
            web3 = new Web3(ethereum);
            try {
                // ask user for permission
                await ethereum.enable();
                // user approved permission
                console.log('[WebRTC/VoIP Blockchain] user approved permission');
            } catch (error) {
                // user rejected permission
                console.log('[WebRTC/VoIP Blockchain] user rejected permission');
            }
        }
        // Old web3 provider
        else if (window.web3) {
            web3 = new Web3(web3.currentProvider);
            // no need to ask for permission
            console.log('[WebRTC/VoIP Blockchain]  old web3 , not needed permission');
        }
        // No web3 provider
        else {
            console.log('No web3 provider detected');
        }

        console.log("[WebRTC/VoIP Blockchain] currentProvider - ", web3.currentProvider);
        console.log("[WebRTC/VoIP Blockchain] Web3 version ", web3.version);


        // Load contract data
        var contractAddress = '0x62481bdbfdf04a00921e619160566ccdf3e3e1fd';
        var abi = [{
            "constant": false,
            "inputs": [
                {"internalType": "string", "name": "sender", "type": "string"},
                {"internalType": "string", "name": "receiver", "type": "string"},
                {"internalType": "string", "name": "datetime", "type": "string"}],
            "name": "New_Trans",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        }, {
            "constant": true,
            "inputs": [],
            "name": "readAlltrans",
            "outputs": [{
                "components": [{
                    "internalType": "uint256",
                    "name": "tranxid",
                    "type": "uint256"
                }, {"internalType": "string", "name": "sender", "type": "string"},
                    {"internalType": "string", "name": "receiver", "type": "string"},
                    {"internalType": "string", "name": "Date_Time", "type": "string"}],
                "internalType": "struct Ast.Transaction[]",
                "name": "",
                "type": "tuple[]"
            }],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        }, {
            "constant": true,
            "inputs": [{"internalType": "uint256", "name": "id", "type": "uint256"}],
            "name": "read_trans",
            "outputs": [{
                "components": [{
                    "internalType": "uint256",
                    "name": "tranxid",
                    "type": "uint256"
                }, {"internalType": "string", "name": "sender", "type": "string"}, {
                    "internalType": "string",
                    "name": "receiver",
                    "type": "string"
                }, {"internalType": "string", "name": "Date_Time", "type": "string"}],
                "internalType": "struct Ast.Transaction",
                "name": "",
                "type": "tuple"
            }],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        }];

        //contract instance
        var contract;
        try {
            // web3 < 1.0 which support c small instead of C
            contract = new web3.eth.Contract(abi, contractAddress);
            console.log("[WebRTC/VoIP Blockchain] Contract - ", contract);
        } catch (e) {
            console.error("[WebRTC/VoIP Blockchain] could not create instance of contract ");
            console.error(e);
        }


        // Accounts
        var account;
        try {
            web3.eth.getAccounts(function (err, accounts) {
                if (err != null) {
                    alert("Error retrieving accounts.");
                    return;
                }
                if (accounts.length == 0) {
                    alert("No account found! Make sure the Ethereum client is configured properly.");
                    return;
                }
                account = accounts[0];
                console.log('[WebRTC/VoIP Blockchain] Account: ', account);
                web3.eth.defaultAccount = account;
            });
        } catch (e) {
            console.error(e);
        }


        //Smart contract functions
        function updateCDR() {
            caller = $("#caller").val() || "altanai";
            callee = $("#callee").val() || "jack";
            datetimestamp = Date();

            contract.methods.New_Trans(caller, callee, datetimestamp)
                .send({from: account})
                .then(function (tx) {
                    console.log("[WebRTC/VoIP Blockchain] Transaction: ", tx);
                    $("#info").html(tx);
                    $("#status").val('CDR written');
                    alert("CDR written");
                });
        }

        function readCDR() {
            contract.methods.readAlltrans()
                .call()
                .then(function (info) {
                    console.log("info: ", info);
                    $("#info").html(info);
                    $("#status").val('CDR fetched');
                    alert("CDR fetched");
                });
        }

        $("#buttonUpdate").click(_ => {
            console.log("[WebRTC/VoIP Blockchain] buttonUpdate");
            updateCDR();
        });

        $("#buttonFetch").click(_ => {
            console.log("[WebRTC/VoIP Blockchain] buttonFetch");
            readCDR();
        });

    });