# Decentralize VoIP records using Blockchain

CDR dAPP is distributed across peers in SIP-WebRTC-Voice over IP such as Freeswitch , asterisk, kamailio , webrtc communication platforms ...
Call Data Records are batched together and pushed to a contract in a Blockchain network. 
Updated smart contract is pushed to RSK blockchain thus making them immutable for all future references.

### What are we doing ?
De-centralize call records using Blockchain and Smart contracts

## Process 

1. Batch together call transactional CDRs
2. Deploy VoIP transaction on Blockchain network 
3. Update Call state in smart contract

## Why are we doing this ?

Existing methods of CDR ( Call Detail Records ) management are subjected to
mismanagement , ransomware attacks , data breaches and provide no transparency or authentication .

## Technology 

**Web Phone**
- WebRTC browser API
- JSSIP - SIP protocol over Websockets
- Web3.js - Ethereum JSON-RPC API
- Metamask - Browser Extension

**VoIP Network**
- Kamailio SIP server

**Blockchain Network**
- RSK smart contract

## POC outline 

![poc](screenshots/CDR%20dAPP%20blockchain%202%20part%20call%20by%20altanai.jpg)

### Step 1 : Create Smart contract on RSK via remix

Metamask is web wallet which facilitates transactions using your account. 
Using chrome extension it was plugin into webrtc compatible browser like Chrome , Mozilla , Opera 
Using Remix and Metamask  to create and deploy a simple smart contract on RSK’s Testnet


####  1.1 : Configure RSK Testnet in Metamask as new RPC network  
Use url https://public-node.testnet.rsk.co

![img](screenshots/Screenshot%20from%202020-11-05%2018-54-35.png)

#####  1.2 : Obtain Testnet R-BTC at faucet.testnet.rsk.co.
#####        - After transaction is complete , see  0.05 R-BTC in metamask 

![img](screenshots/Screenshot%20from%202020-11-05%2019-02-02.png)

####  1.3: Import the sol file containing the VoIP CDR contract 

![img](screenshots/Screenshot%20from%202020-11-05%2019-22-09.png)

####  1.4 : Compile the contract on 

![img](screenshots/Screenshot%20from%202020-11-05%2019-14-10.png)

####  1.5 : Deploy and Run using Inject Web3 

---

### Step 2 : Make a WebRTC Call via SIP Server

#### 2.1. Making a JSSIP WebRTC call

![webrtc](screenshots/Screenshot%20from%202020-11-08%2010-33-07.png)

![webrtc](screenshots/Screenshot%20from%202020-11-08%2010-35-56.png)


#### 2.2. Call signalled via kamailio SIP Server 

![kamailio](screenshots/Screenshot from 2020-11-08 10-39-41.png) 

---

### Step 3 : Call Transaction is updated on Smart Contract by WebRTC client

![rsk](screenshots/Screenshot%20from%202020-11-08%2010-38-52.png)
Fig : WebRTC stats for Call on chrome://webrtc-internals 

![rsk](screenshots/Screenshot%20from%202020-11-08%2011-11-19.png)
Fig : Fetching the values from updated smart contract for CDRs

---

### Step 4 : Can be monitored on RSK Explorer

![rsk](screenshots/Screenshot%20from%202020-11-08%2010-37-50.png)
Fig : https://explorer.testnet.rsk.co/tx/0xc9f85a276426fa987adc36e206007e3eb9ce37d1c587a58a5f63efcb445f9071
