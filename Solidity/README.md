# Smart Contract 

Smart contracts are coded and digitally recorded on the Blockchain, 
making them trustless, autonomous, and self-sufficient.

![rsk](../screenshots/De-centralize%20call%20records%20using%20Blockchain%20and%20Smart%20contracts.jpg)


## RSK Infrastructure Framework Open Standard (RIF OS) 

Suite of open and decentralized infrastructure protocols that enable faster, easier and scalable development of distributed applications (dApps) within a unified environment.

## Deploy RSK testnet

Install Ubuntu node for RSK and start the service 
```shell script
sudo service rsk status
● rsk.service - RSK Node
   Loaded: loaded (/lib/systemd/system/rsk.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2020-11-04 15:31:59 IST; 18h ago
 Main PID: 7544 (java)
    Tasks: 61 (limit: 4915)
   CGroup: /system.slice/rsk.service
           └─7544 /usr/bin/java -Xss4M -Dlogback.configurationFile=/etc/rsk/logback.xml -cp /usr/share/rsk/rsk.jar co.rsk.Start 2>&1

Nov 04 15:31:59 altanai-Inspiron-15-5578 systemd[1]: Started RSK Node.
```

## Contract Application Binary Interface (ABI)

```json
[
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "string",
				"name": "sender",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "receiver",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "datetime",
				"type": "string"
			}
		],
		"name": "New_Trans",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "readAlltrans",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "tranxid",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "sender",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "receiver",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Time",
						"type": "string"
					}
				],
				"internalType": "struct Ast.Transaction[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "id",
				"type": "uint256"
			}
		],
		"name": "read_trans",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "tranxid",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "sender",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "receiver",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Time",
						"type": "string"
					}
				],
				"internalType": "struct Ast.Transaction",
				"name": "",
				"type": "tuple"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]
```

## Development Tools 

### Ganache - perosnal blockchain for Ethereum development.

1. Download - https://www.trufflesuite.com/ganache
2. change permission 
```shell script
chmod a+x Ganache-2.0.1.AppImage
```
3. RUn the image 
```shell script
./Ganache-2.0.1.AppImage
```

### Truffle - development environment

testing framework and asset pipeline for blockchains 
using the Ethereum Virtual Machine (EVM)

```shell script
➜  truffle git:(staging) ✗ npx truffle version
Truffle v5.1.51 (core: 5.1.51)
Solidity - 0.4.24 (solc-js)
Node v12.16.1
Web3.js v1.2.9
```

## web3.js - Ethereum JavaScript API

JS API which connects to the Generic JSON-RPC spec.

Via nodejs 
```shell script
npm install web3
```

or VIA CDN 
```html
<script src="https://cdn.jsdelivr.net/npm/web3@latest/dist/web3.min.js"></script>
```


## References 

- https://www.trufflesuite.com/ganache
